# Create from 3 to 5 length symbols using all characters
```
crunch 3 5 0123456789abcdefghijklmnopqrstuvwxyz -o Documents/pass.txt
crunch 3 5 0123456789abcdefghijklmnopqrstuvwxyz -o pass.txt
```

# Just redirect the output of Crunch into the file.

```
crunch 3 5 0123456789abcdefghijklmnopqrstuvwxyz >> /usr/share/wordlists/rockyou.txt
```
# All combination of eight digit
```
crunch 8 8 12345678 -o wordlist.txt
```
